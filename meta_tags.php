<meta charset="utf-8">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Hind+Vadodara:wght@300&family=Montserrat:wght@300&family=Raleway:wght@300&family=Roboto:wght@300&display=swap" rel="stylesheet">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="theme-color" content="#326BF3" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="shortcut icon" href="logos/mp.png" type="image/x-icon">
<link rel="icon" href="logos/mp.png" type="image/x-icon">
<link href="css/google-sans.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/menu.css" rel="stylesheet">
<link id="effect" href="css/dropdown-effects/fade-down.css" media="all" rel="stylesheet">
<link href="css/magnific-popup.css" rel="stylesheet">
<link href="css/owl.carousel.min.css" rel="stylesheet">
<link href="css/flexslider.css" rel="stylesheet">
<link href="css/owl.theme.default.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link href="css/preloader.css" rel="stylesheet">